const mongoose = require('mongoose')
const Building = require('../models/Building')

mongoose.connect('mongodb://localhost:27017/example')

async function clear () {
  await Building.deleteMany({})
}

async function main () {
  await clear()
  Building.insertMany([{
    name: 'ตึกคณะวิทยาการสารสนเทศ',
    level: 11
  },
  {
    name: 'ตึกสิรินธร',
    level: 5
  }
  ])
}
main().then(function () {
  console.log('Finish')
})
